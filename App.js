import React from "react";
import { NativeBaseProvider, Box } from "native-base";
import { Dimensions } from "react-native";
import Dashboard from "./components/Dashboard/Dashboard";
import Footer from "./components/Footer/Footer";
let ScreenHeight = Dimensions.get("window").height;

export default function App() {
  return (
    <NativeBaseProvider>
      <Box
        flexDirection="column"
        justifyContent="space-between"
        bg="#ecfbfa"
        minH={ScreenHeight}
      >
        <Dashboard />
        <Footer />
      </Box>
    </NativeBaseProvider>
  );
}
