import React from "react";
import {
  VStack,
  HStack,
  Button,
  IconButton,
  Icon,
  Text,
  NativeBaseProvider,
  Center,
  Box,
  StatusBar,
} from "native-base";
import { MaterialIcons, SimpleLineIcons } from "@expo/vector-icons";

//App Bar Components

function AppBar() {
  return (
    <>
      <StatusBar backgroundColor="#3700B3" barStyle="light-content" />
      <Box safeAreaTop backgroundColor="#FFFFFF" />
      <HStack
        bg="#FFFFFF"
        px="1"
        py="3"
        justifyContent="space-between"
        alignItems="center"
      >
        <HStack space="4" alignItems="center">
          <IconButton
            icon={
              <Icon
                size="sm"
                as={<MaterialIcons name="menu" />}
                color="#1B98A4"
              />
            }
          />
        </HStack>
        <Text color="#1B98A4" fontSize="18" fontWeight="700">
          Dashboard
        </Text>
        <HStack space="1">
          <IconButton
            icon={
              <Icon
                as={<SimpleLineIcons name="location-pin" />}
                size="sm"
                color="#1B98A4"
              />
            }
          />
        </HStack>
      </HStack>
    </>
  );
}

export default AppBar;
