import React from "react";
import { Image, HStack, Text, Center, Flex, Box } from "native-base";
import { StyleSheet } from "react-native";
import AppBar from "./Appbar";
import Pro from "../slider/Carousel";

const Dashboard = () => {
  return (
    <>
      <AppBar />
      <HStack paddingTop={10} paddingLeft={3} space={2}>
        <Box flexDirection="row" justifyContent="space-between">
          <Image
            size={45}
            alt="avatar"
            source={require("./Avatar.png")}
            fallbackSource={require("./Avatar.png")}
          />
        </Box>
        <Box
          style={styles.cardShadow}
          bg="#FFFFFF"
          rounded="lg"
          overflow="hidden"
          maxW="80"
          w="100%"
          h="100%"
          padding="3"
          borderTopLeftRadius={0}
          borderRadius={20}
        >
          <Text fontWeight="bold" fontSize="md" color="#1b98a4">
            Welcome back David!{" "}
            <Text fontSize="md" color="#3e6789">
              Take a look at what's been going on while you were away!
            </Text>
          </Text>
        </Box>
      </HStack>
      <HStack paddingTop={10} paddingLeft={3} space={2}>
        <Box flexDirection="row" justifyContent="space-between">
          <Image
            size={45}
            alt="avatar"
            source={require("./Avatar.png")}
            fallbackSource={require("./Avatar.png")}
          />
        </Box>
        <Box
          style={styles.cardShadow}
          bg="#FFFFFF"
          rounded="lg"
          overflow="hidden"
          maxW="80"
          w="100%"
          h="100%"
          padding="3"
          borderTopLeftRadius={0}
          borderRadius={20}
        >
          <Text fontSize="md" color="#3e6789">
            Your incoming requests are piling up! Why don't you quickly sort
            through them?
          </Text>
        </Box>
      </HStack>
      <Pro />
    </>
  );
};

export default Dashboard;

const styles = StyleSheet.create({
  cardShadow: {
    borderRadius: 16,
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3
  },
  cardContainer: {
    backgroundColor: "#fff",
    borderRadius: 16,
    overflow: "hidden"
  }
});
