import React from "react";
import {
  NativeBaseProvider,
  Box,
  Text,
  Icon,
  HStack,
  Center,
  Pressable,
  Image,
} from "native-base";
import { MaterialCommunityIcons, MaterialIcons, SimpleLineIcons, Ionicons } from "@expo/vector-icons";
import { StyleSheet } from 'react-native';


export default function Footer() {
  const [selected, setSelected] = React.useState(1);
  return (
    <NativeBaseProvider>
      <Box flex={1} bg="#fff" safeAreaTop>
        <Center flex={1}></Center>
        <HStack bg="#fff" alignItems="center" safeAreaBottom shadow={9}>
          <Pressable
            opacity={selected === 0 ? 1 : 0.5}
            py="3"
            flex={1}
            onPress={() => setSelected(0)}
          >
            <Center>
              <Icon
                mb="1"
                as={
                  <SimpleLineIcons
                    name={selected === 0 ? "fire" : "fire"}
                  />
                }
                color="#69879C"
                size="sm"
              />
              <Text color="#69879C" fontSize="12">
                Hot Deals
              </Text>
            </Center>
          </Pressable>
          <Pressable
            opacity={selected === 1 ? 1 : 0.5}
            py="2"
            flex={1}
            onPress={() => setSelected(1)}
          >
            <Center>
              <Icon
                mb="1"
                as={<SimpleLineIcons name="people" />}
                color="#69879C"
                size="sm"
              />
              <Text color="#69879C" fontSize="12">
                Connect
              </Text>
            </Center>
          </Pressable>
          <Image
            style={styles.homeBtn}
            size={90}
            alt="location"
            source={require("./inactive.png")}
            fallbackSource={require("./inactive.png")}
          />
          <Pressable
            opacity={selected === 2 ? 1 : 0.6}
            py="2"
            flex={1}
            onPress={() => setSelected(2)}
          >
            <Center>
              <Icon
                mb="1"
                as={
                  <MaterialCommunityIcons
                    name={selected === 2 ? "card-bulleted-settings" : "card-bulleted-outline"}
                  />
                }
                color="#69879C"
                size="sm"
              />
              <Text color="#69879C" font="12">
                D-Card
              </Text>
            </Center>
          </Pressable>
          <Pressable
            opacity={selected === 3 ? 1 : 0.5}
            py="2"
            flex={1}
            onPress={() => setSelected(3)}
          >
            <Center>
              <Icon
                mb="1"
                as={
                  <Ionicons
                    name={selected === 3 ? "briefcase-sharp" : "briefcase-outline"}
                  />
                }
                color="#69879C"
                size="sm"
              />
              <Text color="#69879C" fontSize="12">
                Experience
              </Text>
            </Center>
          </Pressable>
        </HStack>
      </Box>
    </NativeBaseProvider>
  );
}


const styles = StyleSheet.create({
  homeBtn: {
    position: "relative",
    bottom: 25
  },
  });