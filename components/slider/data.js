let SliderData = [
  {
    id: "3",
    case: "Calendar Invite",
    name: "Robert Kreuzinger",
    image: "",
    backgroundImg: require("./img/image.png"),
    date: ["10", "June"]
  },
  {
    id: "2",
    case: "Group Invite",
    backGround: "#ffeeb6",
    name: "",
    image: require("./img/Group.png"),
    backgroundImg: "",
    date: ""
  },

  {
    id: "1",
    case: "Friend Invite",
    name: "Daniela France",
    backGround: "#b5e4f6",
    image: require("./img/Friend.png"),
    backgroundImg: "",
    date: ""
  }
];
export default SliderData;
