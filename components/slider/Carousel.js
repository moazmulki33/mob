import React, { Component } from "react";
import { View, Dimensions, StyleSheet, ImageBackground } from "react-native";
import {
  Box,
  Heading,
  AspectRatio,
  Image,
  Text,
  HStack,
  Stack,
  Button
} from "native-base";
import Carousel from "react-native-snap-carousel";
import SliderData from "./data.js";

const SLIDER_WIDTH = Dimensions.get("window").width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 3 / 3);

export default class Pro extends Component {
  state = {
    index: 0
  };

  constructor(props) {
    super(props);
    this._renderItem = this._renderItem.bind(this);
  }

  _renderItem({ item }) {
    return (
      <Box
        maxW="80"
        rounded="lg"
        overflow="hidden"
        borderColor="coolGray.200"
        borderWidth="1"
        _dark={{
          borderColor: "coolGray.600",
          backgroundColor: "gray.700"
        }}
        _web={{
          shadow: 2,
          borderWidth: 0
        }}
        _light={{
          backgroundColor: "gray.50"
        }}
      >
        <ImageBackground
          style={styles.backGroundImg}
          source={item.backgroundImg}
        >
          <Box backgroundColor={item.backGround}>
            {item.date !== "" ? (
              <Box
                bg="violet.500"
                _dark={{
                  bg: "violet.400"
                }}
                _text={{
                  color: "warmGray.50",
                  fontWeight: "700",
                  fontSize: "xs"
                }}
                position="relative"
                top="5"
                left="5"
                px="3"
                py="1.5"
                w={20}
                borderRadius={10}
              >
                {item.date}
              </Box>
            ) : null}
            <AspectRatio style={styles.avatar} w="100%" ratio={16 / 9}>
              <Image source={item.image} alt="friend" />
            </AspectRatio>
          </Box>
        </ImageBackground>

        <Box style={styles.dataBox}>
          <Stack style={styles.actionBox} p="4" space={3}>
            <Stack space={2}>
              <Heading size="md" ml="-1">
                {item.case}
              </Heading>
              <Text
                fontSize="xs"
                _light={{
                  color: "#3E6789"
                }}
                _dark={{
                  color: "#3E6789"
                }}
                fontWeight="600"
                ml="-0.5"
                mt="-1"
              >
                {item.name}
              </Text>
            </Stack>
            <HStack
              alignItems="center"
              space={4}
              justifyContent="space-between"
            >
              <HStack>
                <Text
                  color="coolGray.600"
                  _dark={{
                    color: "warmGray.200"
                  }}
                  fontWeight="400"
                />
                <Box style={styles.btnBox}>
                  <Button
                    variant="ghost"
                    onPress={() => {
                      this._carousel.snapToPrev();
                    }}
                  >
                    Reject
                  </Button>
                  <Button
                    onPress={() => {
                        
                    }}
                    w="20"
                    borderRadius="30"
                  >
                    Accept
                  </Button>
                </Box>
              </HStack>
            </HStack>
          </Stack>
        </Box>
      </Box>
    );
  }

  render() {
    return (
      <Box style={styles.BigBox} paddingBottom="20">
        <Carousel
          layout={"stack"}
          ref={c => {
            this._carousel = c;
          }}
          data={SliderData}
          renderItem={this._renderItem}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH - 80}
          containerCustomStyle={styles.carouselContainer}
          firstItem="2"
          itemWidth={SLIDER_WIDTH - 100}
          itemHeight={ITEM_HEIGHT - 10}
          lockScrollWhileSnapping={true}
          layoutCardOffset={15}
        />
      </Box>
    );
  }
}

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 50
  },
  itemContainer: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "dodgerblue"
  },
  itemLabel: {
    color: "white",
    fontSize: 24
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center"
  },
  avatar: {
    position: "relative",
    top: 40,
    left: 25
  },
  dataBox: {
    backgroundColor: "white",
    height: 100,
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  actionBox: {
    backgroundColor: "#fafafb",
    position: "relative",
    width: 250,
    height: 125,
    bottom: 35,
    borderRadius: 15
  },
  reject: {
    color: "black"
  },
  backGroundImg: {
    maxHeight: 175,
    height: 175
  },
  btnBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  BigBox: {
    display: "flex",
    flexDirection: "row-reverse"
  },
  right: {
    direction: "rtl"
  }
});
